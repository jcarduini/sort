@profile
def mergeSort(lista):

     if len(lista)>1:
         metade = len(lista)//2
         metade_esquerda = lista[:metade]
         metade_direita = lista[metade:]

         mergeSort(metade_esquerda)
         mergeSort(metade_direita)

         i=0
         j=0
         k=0
         while i < len(metade_esquerda) and j < len(metade_direita):
             if metade_esquerda[i] < metade_direita[j]:
                 lista[k]=metade_esquerda[i]
                 i=i+1
             else:
                 lista[k]=metade_direita[j]
                 j=j+1
             k=k+1

         while i < len(metade_esquerda):
             lista[k]=metade_esquerda[i]
             i=i+1
             k=k+1

         while j < len(metade_direita):
             lista[k]=metade_direita[j]
             j=j+1
             k=k+1