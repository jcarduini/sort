@profile
def radixSort(lista):
    RADIX = 10
    tamanhoMax = False
    tmp , placement = -1, 1

    while not tamanhoMax:
        tamanhoMax = True
        # declara e inicializa os buckets
        buckets = [list() for _ in range( RADIX )]

        # divide a lista de entrada
        for  i in lista:
            tmp = i / placement
            buckets[tmp % RADIX].append( i )
            if tamanhoMax and tmp > 0:
                tamanhoMax = False

        a = 0
        for b in range( RADIX ):
            buck = buckets[b]
            for i in buck:
                lista[a] = i
                a += 1

        placement *= RADIX