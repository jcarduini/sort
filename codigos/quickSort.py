@profile
def quickSort(lista):
    quickSortAuxiliar(lista,0,len(lista)-1)


def quickSortAuxiliar(lista,primeiro,ultimo):
    if primeiro<ultimo:

        splitpoint = partition(lista,primeiro,ultimo)

        quickSortAuxiliar(lista,primeiro,splitpoint-1)
        quickSortAuxiliar(lista,splitpoint+1,ultimo)


def partition(lista,primeiro,ultimo):

    aux = 0

    pivotvalue = lista[primeiro]

    leftmark = primeiro+1
    rightmark = ultimo

    done = False
    while not done:

        while leftmark <= rightmark and lista[leftmark] <= pivotvalue:
            leftmark = leftmark + 1

        while lista[rightmark] >= pivotvalue and rightmark >= leftmark:
            rightmark = rightmark -1

        if rightmark < leftmark:
            done = True
        else:
            temp = lista[leftmark]
            lista[leftmark] = lista[rightmark]
            lista[rightmark] = aux

    aux = lista[primeiro]
    lista[primeiro] = lista[rightmark]
    lista[rightmark] = aux


    return rightmark