@profile
def bucketSort(floats):
    buckets = [ [] for _ in range(len(floats)) ]
    for num in floats:
        i = int(len(floats) * num)
        print(buckets)
        buckets[i].append(num)

    result = []
    for bucket in buckets:
        insertionSort(bucket)# INSERTION_SORT(bucket)
        result += bucket
    return result


# colocamos a mesma versão do Insertion Sort, que já havíamos feito, aqui apenas
# para facilitar a análise de complexidade do Bucket Sort

def insertionSort(lista):
	for j in range(1,len(lista)):
		chave = lista[j]
		i = j
		while (i>0 and lista[i-1]>chave):
			lista[i] = lista[i-1]
			i = i-1
		lista[i] = chave