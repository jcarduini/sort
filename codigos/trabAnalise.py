import numpy.random as nprnd
from time import time
import timeit
import sys
import copy

sys.setrecursionlimit(10000)

def selectionSort(lista):
   for fillslot in range(len(lista)-1,0,-1):
       positionOfMax=0
       for location in range(1,fillslot+1):
           if lista[location]>lista[positionOfMax]:
               positionOfMax = location

       auxiliar = lista[fillslot]
       lista[fillslot] = lista[positionOfMax]
       lista[positionOfMax] = auxiliar


def insertionSort(lista):
   for index in range(1,len(lista)):

     currentvalue = lista[index]
     position = index

     while position>0 and lista[position-1]>currentvalue:
         lista[position]=lista[position-1]
         position = position-1

     lista[position]=currentvalue


def mergeSort(lista):

     if len(lista)>1:
         metade = len(lista)//2
         metade_esquerda = lista[:metade]
         metade_direita = lista[metade:]

         mergeSort(metade_esquerda)
         mergeSort(metade_direita)

         i=0
         j=0
         k=0
         while i < len(metade_esquerda) and j < len(metade_direita):
             if metade_esquerda[i] < metade_direita[j]:
                 lista[k]=metade_esquerda[i]
                 i=i+1
             else:
                 lista[k]=metade_direita[j]
                 j=j+1
             k=k+1

         while i < len(metade_esquerda):
             lista[k]=metade_esquerda[i]
             i=i+1
             k=k+1

         while j < len(metade_direita):
             lista[k]=metade_direita[j]
             j=j+1
             k=k+1


def quickSort(lista):
    quickSortAuxiliar(lista,0,len(lista)-1)


def quickSortAuxiliar(lista,primeiro,ultimo):
    if primeiro<ultimo:

        splitpoint = partition(lista,primeiro,ultimo)

        quickSortAuxiliar(lista,primeiro,splitpoint-1)
        quickSortAuxiliar(lista,splitpoint+1,ultimo)


def partition(lista,primeiro,ultimo):

    aux = 0

    pivotvalue = lista[primeiro]

    leftmark = primeiro+1
    rightmark = ultimo

    done = False
    while not done:

        while leftmark <= rightmark and lista[leftmark] <= pivotvalue:
            leftmark = leftmark + 1

        while lista[rightmark] >= pivotvalue and rightmark >= leftmark:
            rightmark = rightmark -1

        if rightmark < leftmark:
            done = True
        else:
            temp = lista[leftmark]
            lista[leftmark] = lista[rightmark]
            lista[rightmark] = aux

    aux = lista[primeiro]
    lista[primeiro] = lista[rightmark]
    lista[rightmark] = aux


    return rightmark


def countingSort(lista, k ):
    counter = [0] * ( k + 1 )
    for i in lista:
      counter[i] += 1

    ndx = 0;
    for i in range( len( counter ) ):
      while 0 < counter[i]:
        lista[ndx] = i
        ndx += 1
        counter[i] -= 1


def radixSort(lista):
    RADIX = 10
    tamanhoMax = False
    tmp , placement = -1, 1

    while not tamanhoMax:
        tamanhoMax = True
        # declara e inicializa os buckets
        buckets = [list() for _ in range( RADIX )]

        # divide a lista de entrada
        for  i in lista:
            tmp = i / placement
            buckets[tmp % RADIX].append( i )
            if tamanhoMax and tmp > 0:
                tamanhoMax = False

        a = 0
        for b in range( RADIX ):
            buck = buckets[b]
            for i in buck:
                lista[a] = i
                a += 1

        placement *= RADIX


if __name__ == '__main__':

    list1 = [nprnd.randint(0,1000) for r in xrange(2^10)]
    list2 = [nprnd.randint(0,1000) for r in xrange(2^11)]
    list3 = [nprnd.randint(0,1000) for r in xrange(2^12)]
    list4 = [nprnd.randint(0,1000) for r in xrange(2^13)]
    list5 = [nprnd.randint(0,1000) for r in xrange(2^14)]
    list6 = [nprnd.randint(0,1000) for r in xrange(25000)]
    list7 = [nprnd.randint(0,1000) for r in xrange(50000)]
    list8 = [nprnd.randint(0,1000) for r in xrange(100000)]
    list9 = [nprnd.randint(0,1000) for r in xrange(150000)]
    list10 = [nprnd.randint(0,1000) for r in xrange(200000)]
    list11 = [nprnd.randint(0,1000) for r in xrange(500000)]



    print("Insertion Sort:")
    inicioInsertion = time()
    insertionSort(copy.copy(list3))
    fimInsertion = time()
    print(fimInsertion-inicioInsertion)

"""    print("Selection Sort:")
    inicioSelection = time()
    selectionSort(copy.copy(list3))
    fimSelection = time()
    print(fimSelection - inicioSelection)

    print("Merge Sort:")
    inicioMerge = time()
    mergeSort(copy.copy(list3))
    fimMerge = time()
    print(fimMerge - inicioMerge)

    print("Quick Sort:")
    inicioQuick = time()
    quickSort(copy.copy(list3))
    fimQuick = time()
    print(fimQuick - inicioQuick)

    print("Radix Sort:")
    inicioRadix = time()
    radixSort(copy.copy(list3))
    fimRadix = time()
    print(fimRadix - inicioRadix)
"""
    #print("Counting Sort:")
    #inicioCounting = time()
    #countingSort(copy.copy(list3), 100)
    #fimCounting = time()
    #print(fimCounting - inicioCounting)
