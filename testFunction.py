# Serve para importar arquivos em outro diretório
# A CADA NOVO MÉTODO MUDAR O IMPORT,  A CHAMADA DA FUNÇÃO E O SYS.PATH
import sys
sys.path.append('/home/julia/Documentos/Analise/Trabalhos/sort/codigos')

from monitor import *
from ordena import *

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("n", type=int, help="numero de elementos no vetor de teste")
args = parser.parse_args()

v = criavet(args.n)
#Coloca aqui a função que será testada
bubble_sort(v)


## A EXECUÇÃO DESSE ARQUIVO EH ASSIM
## NA LINHA DE COMANDO VC MANDA O NOME DO ARQUIVO E O TAMANHO DO ELEMNTO DO vetor
## EXEMPLO testFuntion.py 10
## ele gera um vetor aleatório (criavet) e manda pro bubble_sort



